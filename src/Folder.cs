﻿using System;
using System.Collections.Generic;
using Subscribe.Contracts;

namespace Subscribe
{
    public class Folder
    {
      
        public Guid Id { get; } = default(Guid);
        public FolderMonitor InstanceMonitor { get; } = default(FolderMonitor);

        public Catalogue RecordCatalogue { get; }


        public Folder(FolderMonitor instance, Catalogue catalogue)
        {
            Id = Guid.NewGuid();
            InstanceMonitor = instance;
            RecordCatalogue = catalogue;
        }


        public class Catalogue
        {
            public string PathFolder { get; }

            public IEnumerable<ISubscriber<FolderMonitor>> Subscribers { get; } 

            public Catalogue(string pathFolder, IEnumerable<ISubscriber<FolderMonitor>> subscribers)
            {
                PathFolder = pathFolder;
                Subscribers = subscribers;
            }
        }
    }
}
