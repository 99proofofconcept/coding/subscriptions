﻿using System;
using System.IO;

namespace Subscribe
{
    public class FolderMonitor : IDisposable
    {

        public delegate void FolderChangeHandler(object sender, FileEventArgs e);
        public event FolderChangeHandler FolderChanged;

        public bool EnableRaisingEvents { private get; set; } = default(bool);

        private FileSystemWatcher fileSystemWatcher;
        private readonly string folderToWatch;


        public FolderMonitor(string folderToWatch)
        {
            this.folderToWatch = folderToWatch;
        }


        public void RunMonitor()
        {
            fileSystemWatcher = new FileSystemWatcher(folderToWatch);
            fileSystemWatcher.EnableRaisingEvents = EnableRaisingEvents;
            fileSystemWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.Size;


            fileSystemWatcher.Created += new FileSystemEventHandler(OnCreated);
            fileSystemWatcher.Changed += new FileSystemEventHandler(OnChanged);
            fileSystemWatcher.Deleted += new FileSystemEventHandler(OnDeleted);
            fileSystemWatcher.Renamed += new RenamedEventHandler(OnRenamed);
        }

      
        private void OnRenamed(object sender, RenamedEventArgs e) => FolderChanged.Invoke(this, InfoFile(e));

        private void OnDeleted(object sender, FileSystemEventArgs e) => FolderChanged.Invoke(this, InfoFile(e));

        private void OnChanged(object sender, FileSystemEventArgs e) => FolderChanged.Invoke(this, InfoFile(e));

        private void OnCreated(object sender, FileSystemEventArgs e) => FolderChanged.Invoke(this, InfoFile(e));


        private FileEventArgs InfoFile(FileSystemEventArgs e)
        {
            return new FileEventArgs()
            {
                Name = e.Name,
                OldName = (e.GetType() == typeof(RenamedEventArgs)) ? ((RenamedEventArgs)e).OldName : e.Name,
                Path = e.FullPath,
                Extension = Path.GetExtension(e.FullPath),
                ChangeTypes = e.ChangeType
            };

        }

        #region IDisposable 

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing && fileSystemWatcher != null)
                {
                    fileSystemWatcher.Dispose();
                    fileSystemWatcher = null;
                }

                disposed = true;
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
