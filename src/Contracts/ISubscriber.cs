﻿namespace Subscribe.Contracts
{
    public interface ISubscriber<in T> where T : class
    {
        void Subscribe(T source);
    }
}
