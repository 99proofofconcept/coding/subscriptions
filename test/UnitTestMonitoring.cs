﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using FakeItEasy;
using Subscribe;
using Subscribe.Contracts;
using Xunit;

namespace TestSubscribe
{
    public class UnitTestMonitoring 
    {
        
        private readonly string path = @"C:\MonitoringTemp";
        private readonly string newpath = @"C:\NewMonitoringTemp";

        private void CreateFolder(string path) { if (!Directory.Exists(path)) Directory.CreateDirectory(path); }
        private void DeleteFolder(string path) { if (Directory.Exists(path)) Directory.Delete(path); }

        
        private Monitoring BuildMonitoring(string path)
        {
            CreateFolder(path);
            IEnumerable<ISubscriber<FolderMonitor>> subsFake = A.Fake<IEnumerable<ISubscriber<FolderMonitor>>>();
            List<Folder.Catalogue> lstCatalogue = new List<Folder.Catalogue>() { new Folder.Catalogue(path, subsFake) };

            return new Monitoring(lstCatalogue);
            
        }
        

        [Fact]
        public void Should_add_a_new_subscription_after_start_monitoring()
        {
            Monitoring monitoring = BuildMonitoring(path);
            monitoring.Start();

            Assert.True(monitoring.LstFolderscatalogue.Count == 1);
            monitoring.Dispose();
            DeleteFolder(path);
        }

        [Fact]
        public void Should_have_2_subscriptions_after_add_another_susbscription()
        {
            Monitoring monitoring = BuildMonitoring(path);
            monitoring.Start();
            Thread.Sleep(1000);
            
            CreateFolder(newpath);
            IEnumerable<ISubscriber<FolderMonitor>> newsubsFake = A.Fake<IEnumerable<ISubscriber<FolderMonitor>>>();
            Folder.Catalogue catalogue = new Folder.Catalogue(newpath, newsubsFake);

            monitoring.Add(catalogue);

            Assert.True(monitoring.LstFolderscatalogue.Count == 2);
            monitoring.Dispose();
            DeleteFolder(path);
            DeleteFolder(newpath);
        }

        [Fact]
        public void Should_have_no_subscriptions_after_start_monitoring_and_remove_the_subscription()
        {
            Monitoring monitoring = BuildMonitoring(path);
            monitoring.Start();   
            Thread.Sleep(1000);

            monitoring.Remove(path);

            Assert.True(monitoring.LstFolderscatalogue.Count ==0);
            monitoring.Dispose();
            DeleteFolder(path);
        }
        
        [Fact]
        public void Should_have_no_subscriptions_after_start_monitoring_and_stop_monitoring()
        {
            Monitoring monitoring = BuildMonitoring(path);
            monitoring.Start();
            Thread.Sleep(1000);

            monitoring.Stop();

            Assert.True(monitoring.LstFolderscatalogue == null);
            monitoring.Dispose();
            DeleteFolder(path);
        }
        
    }
}
